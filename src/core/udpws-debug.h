/*
 * Copyright (c) 2010, Christian Lerche
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Christian Lerche <christian.lerche@uni-rostock.de>
 *
 */

#ifndef UDPWSDEBUG_H_
#define UDPWSDEBUG_H_

#include "udpws-config.h"
/*TODO: necessary? currently the printf define is in the config header but this is
 * not a good place for the "porting layer" */

#define UDPWS_DEBUG_PRINTF(s...)     UDPWS_PRINTF(s);
//#define UDPWS_DEBUG_FUNCTION_NAME    __func__
#define UDPWS_DEBUG_FUNCTION_NAME    ""

/*TODO: make it more general e.g.: DBG_WRITE_LOG
 * but currently only used in the DPWS module */
#if UDPWS_DEBUG_LOG
void log_soap_msg(char* buf, int len);
#define DPWS_LOG_REQUEST(buf, len)        log_soap_msg(buf, len);
#define DPWS_LOG_RESPONSE(buf, len)       log_soap_msg(buf, len);
#else /* UDPWS_DEBUG_LOG */
#define DPWS_LOG_REQUEST(buf, len)
#define DPWS_LOG_RESPONSE(buf, len)
#endif /* UDPWS_DEBUG_LOG */

#define DBG_PRINT_ERROR(s...)
#define DBG_PRINT_ERROR_CHECK(a,s...)
#define DBG_PRINT_WARNING(s...)
#define DBG_PRINT_WARNING_CHECK(a,s...)
#define DBG_PRINT_INFO(s...)
#define DBG_PRINT_INFO_BUF(buf, len)
#define DBG_PRINT_VERBOSE(s...)
#define DBG_PRINT_VERBOSE_BUF(buf, len)
#define DBG_PRINT_ALL(s...)
#define DBG_PRINT_ALL_BUF(buf, len)

#if DEBUG > 0
#undef DBG_PRINT_ERROR
#undef DBG_PRINT_ERROR_CHECK
#define DBG_PRINT_ERROR(s...)               {UDPWS_DEBUG_PRINTF(UDPWS_DEBUG_MODULE" ERROR %s: ", UDPWS_DEBUG_FUNCTION_NAME); UDPWS_DEBUG_PRINTF(s); UDPWS_DEBUG_PRINTF("\n");}
#define DBG_PRINT_ERROR_CHECK(a,s...)       if(a) {DBG_PRINT_ERROR(s)}
#endif /* DEBUG > 0 */

#if DEBUG > 1
#undef	DBG_PRINT_WARNING
#undef	DBG_PRINT_WARNING_CHECK
#define	DBG_PRINT_WARNING(s...)             {UDPWS_DEBUG_PRINTF(UDPWS_DEBUG_MODULE" WARNING %s: ", UDPWS_DEBUG_FUNCTION_NAME); UDPWS_DEBUG_PRINTF(s); UDPWS_DEBUG_PRINTF("\n");}
#define DBG_PRINT_WARNING_CHECK(a,s...)     if(a) {DBG_PRINT_WARNING(s)}
#endif /* DEBUG > 1 */

#if DEBUG > 2
#undef DBG_PRINT_INFO
#undef DBG_PRINT_INFO_BUF
#define DBG_PRINT_INFO(s...)                {UDPWS_DEBUG_PRINTF(UDPWS_DEBUG_MODULE" INFO %s: ", UDPWS_DEBUG_FUNCTION_NAME); UDPWS_DEBUG_PRINTF(s); UDPWS_DEBUG_PRINTF("\n");}
#define DBG_PRINT_INFO_BUF(buf, len)        print_buf(buf, len)
#endif /* DEBUG > 2 */

#if DEBUG > 3
#undef DBG_PRINT_VERBOSE
#undef DBG_PRINT_VERBOSE_BUF
#define DBG_PRINT_VERBOSE(s...)             {UDPWS_DEBUG_PRINTF(UDPWS_DEBUG_MODULE" VERBOSE %s: ", UDPWS_DEBUG_FUNCTION_NAME); UDPWS_DEBUG_PRINTF(s); UDPWS_DEBUG_PRINTF("\n");}
#define DBG_PRINT_VERBOSE_BUF(buf, len)     print_buf(buf, len)
#endif /* DEBUG > 3 */

#if DEBUG > 4
#undef DBG_PRINT_ALL
#undef DBG_PRINT_ALL_BUF
#define DBG_PRINT_ALL(s...)                 {UDPWS_DEBUG_PRINTF(UDPWS_DEBUG_MODULE" DEBUG %s: ", UDPWS_DEBUG_FUNCTION_NAME); UDPWS_DEBUG_PRINTF(s); UDPWS_DEBUG_PRINTF("\n");}
#define DBG_PRINT_ALL_BUF(buf, len)         print_buf(buf, len)
#endif /* DEBUG > 4 */

void print_buf(const char buf[], int buf_len);

#endif /* UDPWSDEBUG_H_ */
