/*
 * Copyright (c) 2010, Christian Lerche
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Christian Lerche <christian.lerche@uni-rostock.de>
 *
 */

#ifndef SENDM_H_
#define SENDM_H_

#include <stdio.h>
#include <stdint.h>

#define SENDM_OK            0
#define SENDM_ERR          -1
/*TODO: BUG in MSP430 GCC - DON'T USE __packed__!*/
#define SENDM_PACKED_STRUCTURE //__attribute__((__packed__));
#define SENDM_STATE_IDLE            0
#define SENDM_STATE_RUN             1
#define SENDM_STATE_FINISHED        2

#ifdef AVR
#define SENDM_RAM            0
#define SENDM_ROM            1
#endif /* AVR */

#define SENDM_END            0
#define SENDM_TOP            1

#define SENDM_ITEM_SIZE    sizeof(struct send_header_s)

struct send_header_s {
    char* buf;
    uint16_t buf_len;
    int8_t next;
#ifdef AVR
int8_t in_progmem; //for data located in ROM (program memory) we need some special routines
/*TODO: to save header size, do something special with the address to indicate ROM memory (e.g. add something)*/
#endif /* AVR */
}SENDM_PACKED_STRUCTURE;

struct sendm_s {
    struct send_header_s *itmes; /*pointer to an array*/
    int8_t max_items;
    int8_t first;
    int8_t last;
    uint8_t used;
    uint8_t error;
    /* TODO: finish event and event parameter */
    int8_t state; //SENDM_STATE_xxx
    uint16_t ack_len; /* 0: all is acked, X: wait for X byte ack */
}SENDM_PACKED_STRUCTURE;

#define sendm_start(sm)                     sm->state = SENDM_STATE_RUN
#define sendm_finished(sm)                 (sm->state == SENDM_STATE_FINISHED)
#define sendm_started(sm)                  (sm->state == SENDM_STATE_RUN)
#define sendm_all_acked(sm)                (sm->ack_len==0)
#define sendm_send_len(sm)                 (sm->ack_len)

#ifdef AVR
#define sendm_add_top_ram(sm, buf, buflen)      sendm_add(sm, buf, buflen, SENDM_TOP, SENDM_RAM)
#define sendm_add_top_rom(sm, buf, buflen)      sendm_add(sm, buf, buflen, SENDM_TOP, SENDM_ROM)
#define sendm_add_end_ram(sm, buf, buflen)      sendm_add(sm, buf, buflen, SENDM_END, SENDM_RAM)
#define sendm_add_end_rom(sm, buf, buflen)      sendm_add(sm, buf, buflen, SENDM_END, SENDM_ROM)
#else
#define sendm_add_top_ram(sm, buf, buflen)      sendm_add(sm, buf, buflen, SENDM_TOP)
#define sendm_add_top_rom(sm, buf, buflen)      sendm_add(sm, buf, buflen, SENDM_TOP)
#define sendm_add_end_ram(sm, buf, buflen)      sendm_add(sm, buf, buflen, SENDM_END)
#define sendm_add_end_rom(sm, buf, buflen)      sendm_add(sm, buf, buflen, SENDM_END)
#endif /* AVR */

/* at init time the sendm needs memory
 * mem_size = max_items * SENDM_ITEM_SIZE */
void sendm_init(struct sendm_s *sm, void* mem, int max_items);
int sendm_add(struct sendm_s *sm, const char buf[], uint16_t buf_len, int in_front
#ifdef AVR
        , int8_t in_progmem
#endif /* AVR */
        );

void sendm_get_send_buf(struct sendm_s *sm, char* buf, uint16_t len);
void sendm_ack_buf(struct sendm_s *sm);
int sendm_get_len(struct sendm_s *sm);

/*TODO: refer TODO in .c file*/
#if DEBUG > 4 /* Debug level is set to ALL */
void sendm_print();
void sendm_test_function(); //remove later
#endif /* DEBUG > 4*/

#endif /* SENDM_H_ */
