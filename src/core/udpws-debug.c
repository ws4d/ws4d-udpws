/*
 * Copyright (c) 2010, Christian Lerche
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Christian Lerche <christian.lerche@uni-rostock.de>
 *
 */

#include "udpws-config.h"
#include <stdio.h>

#define SOAP_LOG_FILENAME         "soap_log.xml"
#define SOAP_LOG_NEW              "\n\n\n"

#if UDPWS_DEBUG_LOG
void log_soap_msg(char* buf, int len) {
    FILE *stream = fopen(SOAP_LOG_FILENAME, "a");

    if (stream == NULL) {
        printf("received_soap_msg(): open log file failed\n");
    }
    //	if (fwrite(SOAP_LOG_NEW, sizeof(SOAP_LOG_NEW), 1, stream ) < 0){
    //		printf("received_soap_msg(): writing failed\n");
    //	}

    if (fwrite(buf, len, 1, stream) < 0) {
        printf("received_soap_msg(): writing failed\n");
    }
    fclose(stream);
}
#endif /* UDPWS_DEBUG_LOG */

void print_buf(const char buf[], int buf_len) {
    int i;
    for (i = 0; i < buf_len; i++) {
        UDPWS_PUTCHAR(*buf);
        buf++;
    } UDPWS_PUTCHAR('\n');
}
