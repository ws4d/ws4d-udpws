/*
 * Copyright (c) 2010, Christian Lerche
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Christian Lerche <christian.lerche@uni-rostock.de>
 *
 */

#ifndef HTTP_H_
#define HTTP_H_

#include <stdint.h>
#include "udpws-config.h"
#include "common.h"
#include "sendm.h"

#define HTTP_OK                       0
#define HTTP_ERROR                   -1

#define HTTP_RET_OK                   200
#define HTTP_RET_BAD_REQUEST          400
#define HTTP_RET_METHOD_NOT_ALLOWED   405
#define HTTP_RET_LENGTH_REQUIRED      411
#define HTTP_RET_SERVER_ERROR         500



#define HTTP_STATE_REQUEST_HEADER     1 /* Request Header */
#define HTTP_STATE_REQUEST_CONT       2 /* Request Content*/
#define HTTP_STATE_PROCESS            3 /* Process Content */
#define HTTP_STATE_RESPONSE           4 /* Generate Response */ /*TODO: REMOVE! NEVER USED!*/
#define HTTP_STATE_CLOSED             5 /* HTTP Session Closed */
/*TODO: when is a http connection closed???
 * a) when the answer is in the sendm or...
 * b) when the tcp connection is closed
 * currently: a)*/

/* This state means: send an error and close connection */
#define HTTP_STATE_ERROR              5

/*
 #define HTTP_STATE_CHUNKED_MODE    0x10
 #define HTTP_STATE_CHUNKED_MODE    0x20
 #define HTTP_STATE_CHUNKED_MODE    0x40
 #define HTTP_STATE_CHUNKED_MODE    0x80
 */

/*TODO: make it possible to use buffer with different sizes?*/
#ifndef HTTP_BUFFER_SIZE
#define HTTP_BUFFER_SIZE    3000
#endif

/*TODO: better alignment */
struct http_con {
    uint8_t state;
    int cont_len; /* CONTENT-LENGTH */
    uint8_t rcvd_req_line; /* Received first line */
    int error; /* last error code */

    /* HTTP BUFFER */
    uint16_t buf_len;
    char buffer[HTTP_BUFFER_SIZE];
};

int http_in(struct http_con *hc, struct sendm_s *sm, char* buf, int len);
int http_init(struct http_con *hc);
void http_new(struct http_con *hc);
int http_closed(struct http_con *hc);
void http_finished_processing(struct http_con *hc, struct sendm_s *sm, int err);

#endif /* HTTP_H_ */
