#the following variables must be defined by the main makefile:
#$(UDPWS_CORE)
#$(FILE2C)
# this is necessary for the PROGMEM AVR fix 
#$(FILE2C_INCLUDE)
#$(UDPWS_DEVICE_GEN)

UDPWS_CORE_DPWS_GEN_FILENAMES=$(shell ls $(UDPWS_CORE)/gen/)
UDPWS_CORE_DPWS_GEN_FILES=$(addprefix $(UDPWS_CORE)/gen/, $(UDPWS_CORE_DPWS_GEN_FILENAMES))
#add gen files of the device and it's hosted services
UDPWS_CORE_DPWS_GEN_FILES+=$(UDPWS_DEVICE_GEN)

#file2c generate configuration
#include file for every generated file
FILE2C_INCLUDE=udpws-gen-include.h
#options: print size, optimize, filter Tabs, CRs and LFs, PROGMEM Fix and include file
FILE2C_FLAGS=-s -o -f TAB -f CR -f LF -p -i $(FILE2C_INCLUDE)
FILE2C_DPWS_HEADER=$(UDPWS_GEN_DIR)/dpws-gen.h
FILE2C_DPWS_SOURCE_PREF=$(UDPWS_GEN_DIR)/dpws-gen_
FILE2C_DPWS_OUTPUT=-h $(FILE2C_DPWS_HEADER) -c $(FILE2C_DPWS_SOURCE_PREF)

UDPWS_GEN_SOURCE_FILES= $(addsuffix .c, $(addprefix dpws-gen_, $(notdir $(UDPWS_CORE_DPWS_GEN_FILES))))

#Files that the Contiki Makefile should delete
CLEAN+=$(FILE2C_DPWS_HEADER)  $(addprefix $(UDPWS_GEN_DIR)/, $(UDPWS_GEN_SOURCE_FILES))


generate: $(FILE2C_DPWS_SOURCE_PREF)

check-generate:
	@echo Check if generated files exists
	@echo ---NOTE: you need to do make generate before you can do make all!---
	ls $(FILE2C_DPWS_HEADER) $(addprefix $(UDPWS_GEN_DIR)/, $(UDPWS_GEN_SOURCE_FILES))
	

#for the dependancy header or source is enough
$(FILE2C_DPWS_SOURCE_PREF) : $(UDPWS_CORE_DPWS_GEN_FILES)
	-mkdir $(UDPWS_GEN_DIR); $(FILE2C) $(FILE2C_FLAGS) $(FILE2C_DPWS_OUTPUT) $(UDPWS_CORE_DPWS_GEN_FILES)
