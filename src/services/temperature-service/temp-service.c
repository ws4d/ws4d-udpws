/*
 * Copyright (c) 2010, Christian Lerche
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Christian Lerche <christian.lerche@uni-rostock.de>
 *
 */

#include "udpws.h"
#include "dpws.h"
#include "dpws-gen.h"

#define DEBUG                 0  /* 0 - No Debug -> 5 - Full Debug */
#define UDPWS_DEBUG_MODULE    "TEMP-SERVICE"
#include "udpws-debug.h"

int get_temp_callback(struct soap_header_s *sh, char* body, int body_len, struct sendm_s *sm, struct stack_s *stack);

#define ACTION_COUNT   1

/*TODO: implement by a real application */
static char temp_str[] = "23";
int temp_str_len = 2;

const struct hosted_service_s udpws_temp_service = {
		ts_path,
		ts_path_len,
		ts_ns_def,
		ts_ns_def_len,
		ts_Type,
		ts_Type_len,
		ts_Type_def,
		ts_Type_def_len,
		ts_ServiceId,
		ts_ServiceId_len,
		ts_Metadata,
		ts_Metadata_len,
		/* Action Array */
		ACTION_COUNT,
		{{ts_uri_action_getIN, ts_uri_action_getIN_len, get_temp_callback}}
};


int get_temp_callback(struct soap_header_s *sh, char* body, int body_len, struct sendm_s *sm, struct stack_s *stack){
	DBG_PRINT_VERBOSE("get_temp_callback() called");
	int err;
	/* SOAP Header */
	err = dpws_gen_header(SH_RELATESTO | SH_TO , NS_WSD | NS_WSDP, udpws_temp_service.namespace_def, udpws_temp_service.namespace_def_len, ts_uri_action_getOUT, ts_uri_action_getOUT_len, sm, sh, NULL);
	if(err){
		return err;
	}
	sendm_add_end_rom(sm, ts_Temperature, ts_Temperature_len);
	sendm_add_end_ram(sm, temp_str, temp_str_len);
	sendm_add_end_rom(sm, ts_xTemperature, ts_xTemperature_len);
	DPWS_FINISH_SOAP(sm);
	UDPWS_PRINTF("Temperature Service: GetTemperature Action called!\n");
	return 0;
}



