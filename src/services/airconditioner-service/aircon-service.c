/*
 * Copyright (c) 2010, Christian Lerche
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Christian Lerche <christian.lerche@uni-rostock.de>
 *
 */

#include "udpws.h"
#include "dpws.h"
#include "dpws-gen.h"
#include <stdlib.h>

#define DEBUG                 3  /* 0 - No Debug -> 5 - Full Debug */
#define UDPWS_DEBUG_MODULE    "ACS"
#include "udpws-debug.h"

int ac_target_temp = 20;
int ac_current_temp = 20;

int acs_GetStatus_callback(struct soap_header_s *sh, char* body, int body_len, struct sendm_s *sm, struct stack_s *stack);
int acs_SetTargetTemperature_callback(struct soap_header_s *sh, char* body, int body_len, struct sendm_s *sm, struct stack_s *stack);

#define ACTION_COUNT   2

const struct hosted_service_s udpws_aircon_service = {
        acs_path,
        acs_path_len,
        acs_ns_def,
        acs_ns_def_len,
        acs_Type,
        acs_Type_len,
        acs_Type_def,
        acs_Type_def_len,
        acs_ServiceId,
        acs_ServiceId_len,
        acs_Metadata,
        acs_Metadata_len,
        /* Action Array */
        ACTION_COUNT, { { acs_uri_GetStatusIn, acs_uri_GetStatusIn_len, acs_GetStatus_callback },
                { acs_uri_SetTargetTemperatureIn, acs_uri_SetTargetTemperatureIn_len, acs_SetTargetTemperature_callback } } };

/* Both actions have the same body BUT not the same header*/
int acs_gen_body(struct sendm_s *sm, struct stack_s *stack) {
    char* target_str;
    char* current_str;
    /* we use the stack to generate the string representation
     * 3 bytes +1 are enough for the string representation as we limit our values (-> set target temp)*/
    target_str = stack_pop(stack, 4);
    current_str = stack_pop(stack, 4);
    if (!(target_str && current_str)) {
        /* one of them is NULL*/
        return DPWS_ERR_MEM;
    }
    itoa2(ac_target_temp, target_str);
    itoa2(ac_current_temp, current_str);

    sendm_add_end_rom(sm, acs_ACState_CurrentTemp, acs_ACState_CurrentTemp_len);
    sendm_add_end_ram(sm, current_str, strlen(current_str));
    sendm_add_end_rom(sm, acs_xCurrentTemp_TargetTemp, acs_xCurrentTemp_TargetTemp_len);
    sendm_add_end_ram(sm, target_str, strlen(target_str));
    sendm_add_end_rom(sm, acs_xTargetTemp_xACState, acs_xTargetTemp_xACState_len);
    DPWS_FINISH_SOAP(sm);
    return 0;
}

int acs_GetStatus_callback(struct soap_header_s *sh, char* body, int body_len, struct sendm_s *sm, struct stack_s *stack) {
    DBG_PRINT_INFO("acs_GetStatus_callback() called");
    int err;
    /* SOAP Header */
    err = dpws_gen_header(SH_RELATESTO | SH_TO, NS_WSD | NS_WSDP, acs_ns_def, acs_ns_def_len, acs_uri_GetStatusOut, acs_uri_GetStatusOut_len, sm, sh, NULL);
    if (err) {
        return err;
    }
    return acs_gen_body(sm, stack);

}

int acs_SetTargetTemperature_callback(struct soap_header_s *sh, char* body, int body_len, struct sendm_s *sm, struct stack_s *stack) {
    DBG_PRINT_INFO("acs_SetTargetTemperature_callback() called");
    int err;
    char* body_end = body + body_len;
    char* p = body;
    uint8_t tag_type;
    /* Parse the body */
    /* go in Body element */
    if (xml_next_tag(p, body_end, NULL, &tag_type, XML_NEXT_TAG_ERR_IF_CLOSE | XML_NEXT_TAG_ERR_IF_EMPTY, &p))
        return DPWS_ERR_PARSE;
    /* go in TargetTemperature element: Error if empty*/
    if (xml_next_tag(p, body_end, NULL, &tag_type, XML_NEXT_TAG_ERR_IF_CLOSE | XML_NEXT_TAG_ERR_IF_EMPTY, &p))
        return DPWS_ERR_PARSE;
    /*here must be the temp string */
    if (tag_type == XML_TAG_TYPE_EMPTY || (p = xml_cont_as_str(p, body_end)) == NULL) {
        /*No target temp in there*/
        DBG_PRINT_INFO("No Target Temperature given -> Set Target Temperature to default: 20\n");
        ac_target_temp = 20;
    } else {
        ac_target_temp = atoi(p);
        /* Limit to max. values (not in wsdl!)*/
        if (ac_target_temp > 40)
            ac_target_temp = 40;
        if (ac_target_temp < -20)
            ac_target_temp = -20;
        DBG_PRINT_INFO("Set Target Temperature to %d\n", ac_target_temp);
    }
    /* SOAP Header */
    err = dpws_gen_header(SH_RELATESTO | SH_TO, NS_WSD | NS_WSDP, acs_ns_def, acs_ns_def_len, acs_uri_SetTargetTemperatureOut,
            acs_uri_SetTargetTemperatureOut_len, sm, sh, NULL);
    if (err) {
        return err;
    }
    return acs_gen_body(sm, stack);
}

