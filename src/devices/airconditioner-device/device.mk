#must be set by every Service Makefile via +=
#MUST be "Simply expanded variables" (NOT recursively)
#http://www.gnu.org/software/automake/manual/make/Flavors.html#Flavors 
UDPWS_SERVICE_SOURCE:=
UDPWS_SERVICE_GEN:=
UDPWS_SERVICE_DIRS:=

#aproach: set the UDPWS_SERVICE_DIR and include the makefile of every service
UDPWS_SERVICE_DIR= $(UDPWS_DEVICE_DIR)/../../services/airconditioner-service
include $(UDPWS_SERVICE_DIR)/aircon-service.mk

#just a helper
UDPWS_DEVICE_SOURCE=device.c
UDPWS_DEVICE_GEN=$(addprefix $(UDPWS_DEVICE_DIR)/gen/, $(shell ls $(UDPWS_DEVICE_DIR)/gen/))
UDPWS_DEVICE_DIRS=$(UDPWS_DEVICE_DIR)

UDPWS_DEVICE_SOURCE+=$(UDPWS_SERVICE_SOURCE)
UDPWS_DEVICE_GEN+=$(UDPWS_SERVICE_GEN)
UDPWS_DEVICE_DIRS+=$(UDPWS_SERVICE_DIRS)
