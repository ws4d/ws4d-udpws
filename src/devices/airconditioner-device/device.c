/*
 * Copyright (c) 2010, Christian Lerche
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Christian Lerche <christian.lerche@uni-rostock.de>
 *
 */

#include "contiki.h"
#include "udpws.h"
#include "dpws.h"
#include "device.h"
#include "dpws-gen.h"

#define DEBUG                 3  /* 0 - No Debug -> 5 - Full Debug */
#define UDPWS_DEBUG_MODULE    "AC"
#include "udpws-debug.h"

//extern const struct hosted_service_s *udpws_aircon_service;
extern const struct hosted_service_s udpws_aircon_service;

extern int ac_target_temp;
extern int ac_current_temp;

#define DEVICE_SERVICE_COUNT           1
#define DEVICE_SERVICES                {&udpws_aircon_service}

PROCESS(aircon_process, "AirConditioner_process");

const struct dpws_device_s udpws_aircon_device = {
		acd_UUID,
		acd_UUID_len,
		acd_ThisDevice,
		acd_ThisDevice_len, /*TODO: acd_ThisDevice_len is not a constant*/
		acd_ThisModel,
		acd_ThisModel_len,
		acd_MetadataVersion,
		acd_MetadataVersion_len,
		acd_ServiceId,
		acd_ServiceId_len,
		&aircon_process,
		DEVICE_SERVICE_COUNT,
		DEVICE_SERVICES
};


static struct etimer periodic_timer;

PROCESS_THREAD(aircon_process, ev, data)
{
    PROCESS_BEGIN();
    etimer_set(&periodic_timer, 3*CLOCK_SECOND);

    while(1) {
      PROCESS_YIELD();
      if (etimer_expired(&periodic_timer)) {
        if (ac_target_temp < ac_current_temp){
        	ac_current_temp--;
        	DBG_PRINT_INFO("cooling (current: %d, target: %d)", ac_current_temp, ac_target_temp);
        }
        if (ac_target_temp > ac_current_temp){
        	ac_current_temp++;
        	DBG_PRINT_INFO("heating (current: %d, target: %d)", ac_current_temp, ac_target_temp);
        }
        etimer_restart(&periodic_timer);
      }
    }
	PROCESS_END();
}








