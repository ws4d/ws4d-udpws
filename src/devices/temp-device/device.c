/*
 * Copyright (c) 2010, Christian Lerche
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Christian Lerche <christian.lerche@uni-rostock.de>
 *
 */

#include "contiki.h"
#include "udpws.h"
#include "dpws.h"
#include "device.h"
#include "dpws-gen.h"

#define DEBUG                 3  /* 0 - No Debug -> 5 - Full Debug */
#define UDPWS_DEBUG_MODULE    "TEMP"
#include "udpws-debug.h"

extern const struct hosted_service_s udpws_temp_service;

#define DEVICE_SERVICE_COUNT           1
#define DEVICE_SERVICES                {&udpws_temp_service}

//PROCESS(temp_process, "Temp_process");

const struct dpws_device_s udpws_temp_device = {
		temp_UUID,
		temp_UUID_len,
		temp_ThisDevice,
		temp_ThisDevice_len, /*TODO: temp_ThisDevice_len is not a constant*/
		temp_ThisModel,
		temp_ThisModel_len,
		temp_MetadataVersion,
		temp_MetadataVersion_len,
		temp_ServiceId,
		temp_ServiceId_len,
		NULL,
		DEVICE_SERVICE_COUNT,
		DEVICE_SERVICES
};


//PROCESS_THREAD(temp_process, ev, data)
//{
//    PROCESS_BEGIN();
//	PROCESS_END();
//}








