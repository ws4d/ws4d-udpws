/*
 * Copyright (c) 2010, Christian Lerche
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Christian Lerche <christian.lerche@uni-rostock.de>
 *
 */

#ifndef UDPWSCONFIG_H_
#define UDPWSCONFIG_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef USE_CONFIG_FILE
#include "config.h"
#endif

#define UDPWS_TARGET_I386
#define UDPWS_PERSISTENT_FILENAME    "persistent.dat"
/* MUST have 45 characters */
#define UDPWS_INITIAL_UUID           "urn:uuid:f11a4a50-3981-11de-80f9-654601168157"

/* Device include*/
#include "device.h"

/*1000-9999 (MUST: 4 bytes)*/
#define UDPWS_PORT                   1234
#define UDPWS_PRINTF(s...)           printf(s)
#define UDPWS_PUTCHAR(c)             putchar(c)

#define UDPWS_WAIT_FOR_START         1

#define UDPWS_MINIMAL_NET_IPV4_BUGFIX

/* The debug levels are: 0 - nothing, 1 - errors, 2 - warnings, 3 - info, 4 - verbose, 5 - debug */
#define UDPWS_DBG_LEVEL_PROCESS      3
#define UDPWS_DBG_LEVEL_DPWS         4
#define UDPWS_DBG_LEVEL_HTTP         1
#define UDPWS_DBG_LEVEL_SENDM        1
#define UDPWS_DEBUG_LOG              1

/* uDPWS Configuration */
#define UDPWS_CONF_WSDISCOVERY                    1
#define UDPWS_CONF_WSDISCOVERY_HELLO              1
#define UDPWS_CONF_WSDISCOVERY_RESOLVE            1
#define UDPWS_CONF_WSDISCOVER_SEND_XADDRS         1
#define UDPWS_CONF_USE_INSTANCEID_AS_METADATAVER  1

#define UDPWS_TRANSFER_GET                        1
/*Size of Cache: UDPWS_RETRANS_CACHE_SIZE * RETRANS_MAX_MSGID_LEN (default: 45)*/
#define UDPWS_RETRANS_CACHE_SIZE                  4

#define UDPWS_TIME_MEASUREMENT                    0
#if UDPWS_TIME_MEASUREMENT
/*TODO: copied from avr*/

#define UDPWS_TIMING_MAX                          100
#define UDPWS_TIMER_PRINTF(s...)                  UDPWS_PRINTF(s)

#define UDPWS_INIT_TIMER()                        init_timer()
#define UDPWS_START_TIMER()                       TCNT2 = 0
#define UDPWS_TAKE_TIME(text)                     timing.val[timing.index] = TCNT2; timing.string[timing.index] = text; timing.index++;
#define UDPWS_PRINT_TIME_LIST()                   print_time_list()
#define UDPWS_RESET_TIME_LIST()                   timing.index=0

extern struct udpws_timing_s timing;
void init_timer();
void inline take_time(char* str);
void print_time_list();

#else /* UDPWS_TIME_MEASUREMENT */
#define UDPWS_TIMING_MAX
#define UDPWS_TIMER_PRINTF(s...)

#define UDPWS_INIT_TIMER()
#define UDPWS_START_TIMER()
#define UDPWS_TAKE_TIME(string)
#define UDPWS_PRINT_TIME_LIST()
#define UDPWS_RESET_TIME_LIST()
#endif /* UDPWS_TIME_MEASUREMENT */

#endif /* UDPWSCONFIG_H_ */
