.DEFAULT_GOAL=all

#you need to 'make generate' first
#after that you can 'make all' or just 'make'

TARGET=sky

UDPWS_CORE=$(UDPWS_PLATFORM_DIR)/../../core
UDPWS_GEN_DIR= ./gen
FILE2C=$(UDPWS_PLATFORM_DIR)/../../../file2c/file2c
include $(UDPWS_CORE)/core.mk

include $(UDPWS_DEVICE_DIR)/device.mk

CONTIKI_SOURCEFILES = $(UDPWS_CORE_SOURCE) $(UDPWS_DEVICE_SOURCE) $(UDPWS_GEN_SOURCE_FILES)
CFLAGS += -O0 -DUIP_MULTICAST 
#-ffunction-sections
#LDFLAGS +=  -Xlinker --gc-sections
LDFLAGS +=  -Xlinker --cref
#TODO: There is a bug in msp430-gcc when using -fdata-sections 
    #-fdata-sections functionality is not implemented
    #refer: http://osdir.com/ml/hardware.texas-instruments.msp430.gcc.user/2004-01/msg00021.html
    
SOURCEDIRS=$(UDPWS_PLATFORM_DIR) $(UDPWS_CORE_DIRS) $(UDPWS_GEN_DIR) $(UDPWS_DEVICE_DIRS)
CONTIKIDIRS=$(UDPWS_PLATFORM_DIR) $(UDPWS_CORE_DIRS) $(UDPWS_GEN_DIR) $(UDPWS_DEVICE_DIRS)


#Files that the Contiki Makefile should delete
CLEAN+=-rf gen ./udpws.minimal-net ./symbols.* soap_log.xml

UIP_CONF_IPV6 = 1
DEFINES=WITH_UIP6
#UIP_CONF_IPV6_CHECKS = 1

include $(UDPWS_CORE)/core-generate.mk

.PHONY: all
all:
	$(MAKE) generate; $(MAKE) project
	
project: check-generate $(CONTIKI_PROJECT) remove-symbols
	
#cleanall: clean
#	-rmdir gen

remove-symbols:
	-rm -rf symbols.*

# Don't include contiki makefile for several targets
ifneq ($(MAKECMDGOALS), )
ifneq ($(MAKECMDGOALS), generate)
ifneq ($(MAKECMDGOALS), all)
include $(CONTIKI)/Makefile.include
endif
endif
endif

#Platform: SKY
CLEAN+=*.sky
