/*
 * Copyright (c) 2010, Christian Lerche
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Christian Lerche <christian.lerche@uni-rostock.de>
 *
 */



/* this is a dummy file because of some deficiencies in the contiki build system*/
#include "udpws-process.c"

/* platform dependend code */
int read_persistent_data(uint32_t *instance_id){
	/* TODO: use eeprom */
	*instance_id = 200000;
	return 0;
}

int write_persistent_data(uint32_t instance_id){
	/* TODO */
	return 0;
}



#if UDPWS_TIME_MEASUREMENT
struct udpws_timing_s timing;
void init_timer(){
	TCCR2B =  _BV(CS20)| _BV(CS21) | _BV(CS22); //1024 * 0.125uS = 128uS
	timing.index = 0;
}

void print_time_list(){
	int i;
	UDPWS_TIMER_PRINTF("---Timing List---\n");
	if (timing.index >= UDPWS_TIMING_MAX){
		UDPWS_TIMER_PRINTF("ERROR: TIMING OVERFLOW\n");
		return;
	}
	for (i = 0; i < timing.index; i++){
		UDPWS_TIMER_PRINTF("Time %u: %u ticks (%s)\n", i, timing.val[i], timing.string[i]);
	}
}

#endif /* UDPWS_TIME_MEASUREMENT */
